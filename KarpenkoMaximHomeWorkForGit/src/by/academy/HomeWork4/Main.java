package by.academy.HomeWork4;

import by.academy.HomeWork4.datacontainer.DataContainer;
import by.academy.HomeWork4.datacontainer.DataForGenerator;
import by.academy.HomeWork4.datacontainer.animal.Animal;
import by.academy.HomeWork4.datacontainer.animal.AnimalAgeComparator;
import by.academy.HomeWork4.datacontainer.person.Person;
import by.academy.HomeWork4.datacontainer.person.PersonRegistrationComparator;

import java.util.Arrays;


public class Main {

    public static void main(String[] args) {


//Person
        DataContainer<Person> personDataContainer = new DataContainer<>(new Person[1]);
        DataForGenerator dataPerson = new DataForGenerator();
        for (int i = 0; i < 10e5; i++) {
            //for (int i = 0; i < 10; i++) {
            personDataContainer.add(new Person(
                    dataPerson.setNick(),
                    dataPerson.setPassword(),
                    dataPerson.setPersonDataRegistration()));
        }

        System.out.println("Person container: ");
        System.out.println(Arrays.toString(personDataContainer.getData()));
        System.out.println();

        DataContainer.sort(personDataContainer, new PersonRegistrationComparator(), true);

        System.out.println("Person container sort: ");
        System.out.println(Arrays.toString(personDataContainer.getData()));
        System.out.println();


        System.out.println("Person container delete for element:");
        try {
            personDataContainer.delete(personDataContainer.getData()[3]);
            System.out.println(Arrays.toString(personDataContainer.getData()));
            System.out.println();
        } catch (
                ArrayIndexOutOfBoundsException e) {
            System.out.println("This element is missing from the array");
        }

        System.out.println("Person container delete for index:");
        personDataContainer.delete(111);
        System.out.println(Arrays.toString(personDataContainer.getData()));

        System.out.println();
        System.out.println();
        System.out.println();


//Animal
        DataContainer<Animal> animalDataContainer = new DataContainer<>(new Animal[1]);
        DataForGenerator dataAnimal = new DataForGenerator();
        for (int i = 0; i < 10e5; i++) {
            //for (int i = 0; i < 4; i++) {
            animalDataContainer.add(new Animal(
                    dataAnimal.setAnimalAge(),
                    dataAnimal.setNick()));
        }
        System.out.println("Animal container: ");
        System.out.println(Arrays.toString(animalDataContainer.getData()));
        System.out.println();
        DataContainer.sort(animalDataContainer, new AnimalAgeComparator(), true);

        System.out.println("Animal container sort: ");
        System.out.println(Arrays.toString(animalDataContainer.getData()));
        System.out.println();

        System.out.println("Animal container delete for element:");
        try {
            animalDataContainer.delete(animalDataContainer.getData()[4]);
            System.out.println(Arrays.toString(animalDataContainer.getData()));

        } catch (
                ArrayIndexOutOfBoundsException e) {
            System.out.println("This element is missing from the array");
            System.out.println(Arrays.toString(animalDataContainer.getData()));
        }
        System.out.println();

        System.out.println("Animal container delete for index:");
        animalDataContainer.delete(4);
        System.out.println(Arrays.toString(animalDataContainer.getData()));
    }

}






