package by.academy.HomeWork4.datacontainer.animal;

public class Animal {
    private Integer age;
    private String nick;


    public Integer getAge() {
        return age;
    }

    public Animal(Integer age,
                  String nick) {
        this.age = age;
        this.nick = nick;
    }

    @Override//
    public String toString() {
        return "The nickname of the animal " + nick + " and age " + age + " ";
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }
}