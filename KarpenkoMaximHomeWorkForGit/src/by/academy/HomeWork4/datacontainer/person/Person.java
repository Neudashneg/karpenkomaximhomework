package by.academy.HomeWork4.datacontainer.person;

import java.io.Serializable;
import java.time.LocalDate;

public class Person implements Serializable {
    private String firstName;
    private String lastName;
    private String nick;
    private String password;
    private LocalDate registration;
    private LocalDate dateOfBirth;//патом поправить если чо!

    public Person() {

    }


    public LocalDate getRegistration() {
        return registration;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setRegistration(LocalDate registration) {
        this.registration = registration;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getNick() {
        return nick;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public String getPassword() {
        return password;
    }

    public Person(String name,
                  String password,
                  LocalDate registration) {
        this.nick = name;
        this.password = password;
        this.registration = registration;
    }


    @Override
    public String toString() {
        return "The nickname of the user " + getNick()
                + " and date of registration " + getRegistration()
                + " password: " + getPassword();
    }/*
    public String toString() {
        return "Имя " + firstName
                + " фамилия " + lastName
                + " логин " + nick
                + " дата регистрации" + registration
                + " пароль: " + password
                + " дата рождения  " + dateOfBirth
                + " будущего выдающегося студента нашего прекрасного Института";
    }*/

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }
}