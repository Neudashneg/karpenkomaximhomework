package by.academy.HomeWork4.datacontainer.person;

import java.util.Comparator;

public class PersonRegistrationComparator implements Comparator<Person> {
    @Override
    public int compare(Person o1, Person o2) {
        return o1.getRegistration().compareTo(o2.getRegistration());
    }
}