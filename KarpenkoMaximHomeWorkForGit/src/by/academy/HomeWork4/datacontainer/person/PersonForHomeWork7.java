package by.academy.HomeWork4.datacontainer.person;

import java.time.LocalDate;

public interface PersonForHomeWork7 {
    String getPassword();

    LocalDate getRegistration();

    String getNick();
}
