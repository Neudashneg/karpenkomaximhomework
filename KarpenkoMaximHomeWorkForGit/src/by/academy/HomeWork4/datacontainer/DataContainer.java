package by.academy.HomeWork4.datacontainer;

import by.academy.HomeWork4.datacontainer.person.PersonForHomeWork7;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Comparator;

public class DataContainer<E> implements Serializable, PersonForHomeWork7 {


    public E[] getData() {
        return this.data;
    }

    public DataContainer(E[] item) {
        this.data = item;
    }

    public void setData(E[] data) {
        this.data = data;
    }


    private E[] data;


    public void add(E item) {//создаеться null в первой ячейке
        int i = 0;
        do {

            if (getData().length < i + 2) {
                //Exception in thread "main" java.lang.NullPointerException-- не праавильный конструктор был
                //  public DataContainer(Object o) {
                //    }
                //this.item = Arrays.copyOf(this.item, this.item.length * 3 / 2 + 1);
                this.data = Arrays.copyOf(this.data, this.data.length + 1);
            }
            i++;
        } while (this.data[i] != null);
        this.data[i] = item;
        if (this.data[0] == null) {//удаляем null из первой ячейки!
            System.arraycopy(this.data, 1, this.data, 0, this.data.length - 1);
            this.data = Arrays.copyOf(this.data, this.data.length - 1);
        }
    }


    public static <E> void sort(DataContainer<E> item, Comparator<E> comparator, boolean ifNeedSortWriteTrue) {
        E tempData;
        //ifNeedSortWriteTrue = true;//перваначальное условие выполнение сортировки
        while (ifNeedSortWriteTrue) {
            ifNeedSortWriteTrue = false;//условия по которому цикл остановится
            for (int i = 0; i < item.getData().length - 1; i++) {//цикл сверки значений
                if (comparator.compare(item.getData()[i], item.getData()[i + 1]) < 0) {
                    //выполняется когда второе число меньше первого
                    ifNeedSortWriteTrue = true;
                    tempData = item.getData()[i];
                    item.getData()[i] = item.getData()[i + 1];
                    item.getData()[i + 1] = tempData;
                }
            }
        }
    }


    public void delete(int index) {//удаление по индексу из отсортированного массива
        if (index == 0) {
            System.out.println(this.data.length);
            if (this.data.length - 1 >= 0) System.arraycopy(this.data, 1, this.data, 0, this.data.length - 1);
            this.data = Arrays.copyOf(this.data, this.data.length - 1);
        } else if (index > 0 && index < this.data.length - 1) {//1-2
            if (this.data.length - 1 - index >= 0)
                System.arraycopy(this.data, index + 1, this.data, index, this.data.length - 1 - index);
            this.data = Arrays.copyOf(this.data, this.data.length - 1);
        } else if (index == this.data.length - 1) {//3
            this.data = Arrays.copyOf(this.data, this.data.length - 1);
        } else {
            System.out.println("Index outside the array boundaries");
        }
    }

    public void delete(E item) {//удаление по элементу из отсортированного массива
        for (int i = 0; i <= this.data.length; i++) {
            if (this.data[i].equals(item)) {//получаем индекс элемента
                delete(i);//выполняем метод удаления по индексу
                break;//выход из цикла
            }
        }
    }
    public LocalDate getRegistrations() {
        return getRegistration();
    } public String getNicks() {
        return getNick();
    } public String getPasswords() {
        return getPassword();
    }




    @Override
    public LocalDate getRegistration() {
        return null;
    }

    @Override
    public String getNick() {
        return null;
    }

    @Override
    public String getPassword() {
        return null;
    }
}
