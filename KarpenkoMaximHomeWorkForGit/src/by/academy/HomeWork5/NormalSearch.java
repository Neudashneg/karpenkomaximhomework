package by.academy.HomeWork5;

public class NormalSearch implements ISearchEngine {
    public int search(String[] arrayOfString, String searchWord) {
        int numberOfMatchesFound = 0;
        char[] specSymbol = " -,./?'\"\n!:;({[}])".toCharArray();
        for (String s : arrayOfString) {
            /**
             * проверяем игнорируя регистр
             * if(s!=null) проверка строки на null может быть не актуально
             * ранее ловил nullpointer)
             * if (s.length() == searchWord.length())
             *если длина строки искомого равна длине текущей то сравниваем строки и при совпадении увеличиваем счетчик
             *
             * else if (s.length() == searchWord.length() + 1)
             * если данная строка больше на единицу искомой то
             * сравниваем первый элемент искоемого слова с первым или втором символом данной строки
             * при совпадении для первого сравниваем все искомоего слова и
             * сравниваем последний символ данной строки с  спецсимволом
             * при совпадении для второго сравниваем все искомоего слова и
             * сравниваем первый символ данной строки с  спецсимволом
             *
             * else if (s.length() > searchWord.length())
             * оптяь неправильный каст(
             * переделаать с использованием цикла
             * if
             * проверить символ первый  данной строки с первым символом искомого слова
             * else if
             * проверить символ после первого до пред последнего минус длина искомоего слова
             * else if
             * проверить последний символ минус длина искоемого слова
             */
            if (s != null) {
                if (s.length() == searchWord.length()) {
                    if (s.equalsIgnoreCase(searchWord)) {
                        numberOfMatchesFound++;
                    }
                } else if (s.length() == searchWord.length() + 1) {
                    if (s.substring(0, 1).equalsIgnoreCase(searchWord.substring(0, 1))) {
                        if (s.substring(0, searchWord.length()).equalsIgnoreCase(searchWord)) {
                            for (char aChar : specSymbol) {
                                if (aChar == s.charAt(searchWord.length())) {
                                    numberOfMatchesFound++;
                                    break;
                                }
                            }
                        }
                    } else if (s.substring(1, searchWord.length() + 1).equalsIgnoreCase(searchWord)) {
                        for (char aChar : specSymbol) {
                            if (aChar == s.charAt(0)) {
                                numberOfMatchesFound++;
                            }
                        }
                    }
                } else if (s.length() > searchWord.length()) {
                    for (int i = 0; i < s.length() - searchWord.length() + 1; i++) {

                        /*
                                       else if
                         2  проверить символ первый данной строки с первым символом искомого слова
                                              if
                         1  проверить символ после первого до пред последнего минус длина искомоего слова
                                        else
                         3  проверить последний символ минус длина искоемого слова
                         */

                        if (i > 0 && i < s.length() - searchWord.length() &&
                                s.substring(i, i + searchWord.length()).equalsIgnoreCase(searchWord)) {
                            boolean[] checkVar = {false, false};
                            for (char aChar : specSymbol) {
                                if (aChar == s.charAt(i - 1)) {
                                    checkVar[0] = true;
                                }
                                if (aChar == s.charAt(i + searchWord.length())) {
                                    checkVar[1] = true;
                                }
                                if (checkVar[0] & checkVar[1]) {
                                    numberOfMatchesFound++;
                                    break;
                                }
                            }
                        } else if (i == s.length() - searchWord.length() &&
                                s.substring(i, i + searchWord.length()).equalsIgnoreCase(searchWord)) {
                            //последний проверяемый в строке символ
                            for (char aChar : specSymbol) {
                                if (aChar == s.charAt(i - 1)) {
                                    numberOfMatchesFound++;
                                    break;
                                }
                            }
                        } else if (i == 0 && s.substring(0, searchWord.length()).equalsIgnoreCase(searchWord)) {
                            for (char aChar : specSymbol) {
                                if (aChar == s.charAt(searchWord.length())) {
                                    numberOfMatchesFound++;
                                    break;
                                }
                            }
                        }
                    }
                }
            }

        }
        return numberOfMatchesFound;
    }
}
