package by.academy.HomeWork5;

@FunctionalInterface
public interface ISearchEngine {
    /**
     * Написать интерфейс ISearchEngine.
     * Это будет функциональный интерфейс в котором будет метод search
     * который будет принимать два параметра (массив строк в котором мы ищем наше слово, слово которое мы будем искать),
     * а возвращать число (количество найденных совпадений).
     *
     * @return
     */
    int search(String[] arrayOfString, String searchWord);

}
