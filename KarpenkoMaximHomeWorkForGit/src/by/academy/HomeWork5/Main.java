package by.academy.HomeWork5;

import java.nio.file.Files;
import java.nio.file.Paths;

public class Main {
    static String linkForFile =
            "D:\\other\\git_idea\\KarpenkoMaximHomeWorkForGit\\src\\by\\academy\\HomeWork5\\Война и мир.txt";


    public static void main(String[] args) throws Exception {
        //ICalculator calculatorWithCounter = (ICalculator) new CalculatorWithCounter(new CalculatorWithOperator());
        // NormalSearch normalSearch = new NormalSearch() ;
        ISearchEngine normal = new NormalSearch();// что идея творит....
        search(normal);


        ISearchEngine easy = new EasySearch();

        search(easy);
    }

    private static void search(ISearchEngine easySearch) throws Exception {
        int a = easySearch.search(readFileAsString(linkForFile), "война");
        int b = easySearch.search(readFileAsString(linkForFile), "и");
        int c = easySearch.search(readFileAsString(linkForFile), "мир");
        int d = easySearch.search(readFileAsString(linkForFile), "война и мир");
        System.out.println("Война " + a + " и " + b + " мир " + c + " война и мир " + d);
    }


    private static String[] readFileAsString(String fileName) throws Exception {//читайем файл из файла и возвращаем строку
        String data = "";
        data = new String(Files.readAllBytes(Paths.get(fileName)));
        int numberEnter = 0;
        for (int i = 0; i < data.length(); i++) {
            if (data.charAt(i) == '\n') {
                numberEnter++;
            }
        }

        String[] dataArray = new String[numberEnter];
        int j = 0;
        int lengStr = 0;
        for (int i = 0; i < data.length(); i++) {//нада бы уменьшить количество пустых строк
            if (data.charAt(i) == '\n') {//записываем по одной строкой разделенной \n предложению в ячейку
                dataArray[j] = data.substring(lengStr, i);
                lengStr = i;
                if (!dataArray[j].equals("\n   ") & dataArray[j].length() > 3) {
                    j++;
                }
            }
        }
        String[] dataArrayNew = new String[j];
        //System.arrayCopy(from, fromIndex, to, toIndex, count);
        //from - массив, который копируем
        //to - массив в которой копируем
        //fromIndex - индекс в массиве from начиная с которого берем элементы для копирования
        //toIndex - индекс в массиве to начиная с которого вставляем элементы
        //count - количество элементов которые берем из массива from и вставляем в массив to
        //Массив to должен иметь достаточный размер, чтобы в нем уместились все копируемые элементы.
        System.arraycopy(dataArray, 0, dataArrayNew, 0, j);
        return dataArrayNew;
    }
}
