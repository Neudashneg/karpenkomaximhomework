package by.academy.HomeWork5;

public class EasySearch implements ISearchEngine {
@Override
    public int search(String[] arrayOfString, String searchWord) {
        int numberOfMatchesFound = 0;
        char[] specSymbol = " -,./?'\"\n!:;({[}])".toCharArray();
        for (String s : arrayOfString) {
            int index = 0;
            boolean b = false;
            do {
                if (b) {
                    index++;
                }
                index = s.toLowerCase().indexOf(searchWord, index);
                boolean[] checkWord = {false, false};
                if (index != -1) {
                    for (char aChar : specSymbol) {
                        if (index == 0) {
                            checkWord[0] = true;
                        } else {
                            if (aChar == s.charAt(index - 1)) {
                                checkWord[0] = true;
                            }
                        }
                        if (s.length() == searchWord.length()) {
                            checkWord[1] = true;
                        } else if (s.length() ==index+ searchWord.length()) {
                            checkWord[1] = true;
                        } else {
                            if (aChar == s.charAt(index + searchWord.length())) {
                                checkWord[1] = true;
                            }
                        }
                        if (checkWord[0] && checkWord[1]) {
                            numberOfMatchesFound++;
                            break;
                        }
                    }
                    b = true;
                }
            }
            while (index != -1);
        }
        return numberOfMatchesFound;
    }
}
