package by.academy.HomeWork7;


import by.academy.HomeWork7.studentdata.WriteObjectsInFile;

import java.io.*;

public class MainCreateFileOfStudents implements Serializable {

    private static long time = System.nanoTime();//время в нанасекундах на момент инициализации переменной
    private static String linkForFile = "src\\by\\academy\\HomeWork7\\Students";
    private static int number= (int) 10e4;//количесто генераций студентов

    public static void main(String[] args) throws IOException {
        WriteObjectsInFile writeStudents = new WriteObjectsInFile();
        writeStudents.writeObjectStudentInFile(linkForFile,number);
        System.out.println(((double) System.nanoTime() - time) / 10e5+ " милисекунды");
        //~4443.4214 милисекунды выполнялся код
    }



}
