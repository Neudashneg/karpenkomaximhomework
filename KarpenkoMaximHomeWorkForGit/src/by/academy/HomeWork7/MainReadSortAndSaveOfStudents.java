package by.academy.HomeWork7;

import by.academy.HomeWork7.studentdata.ReadStudentsFromAFile;
import by.academy.HomeWork7.studentdata.SortStudents;
import by.academy.HomeWork7.studentdata.WriteFieldSortStudentInFile;
import by.academy.HomeWork7.studentdata.student.Student;
import by.academy.HomeWork7.studentdata.StudentData;

import java.io.*;
import java.util.*;


public class MainReadSortAndSaveOfStudents {

    private static long time = System.nanoTime();
    static private String linkForFile = "src\\by\\academy\\HomeWork7\\Students";
    static private String linkForSortFile = "src\\by\\academy\\HomeWork7\\SortStudents";

    public static void main(String[] args) throws IOException {
        ReadStudentsFromAFile read = new ReadStudentsFromAFile();
        WriteFieldSortStudentInFile write = new WriteFieldSortStudentInFile();
        SortStudents sort = new SortStudents();
        List<Student> students = read.readStudentFromFile(linkForFile);
        write.writeSortedFieldsInFile(sort.sort(students),linkForSortFile);
        //время выполнения кода, по идеи должен работать так: берем время в момент инициализации первой
        //статичной переменой и сравниваем до точки, и получаем время выполнения данного кода
        System.out.println(((double) System.nanoTime() - time) / 10e5 + " милисекунды");//1019045.5218  милисекунды
    }


}


