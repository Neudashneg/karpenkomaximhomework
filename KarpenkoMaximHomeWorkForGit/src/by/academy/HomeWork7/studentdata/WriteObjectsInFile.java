package by.academy.HomeWork7.studentdata;

import by.academy.HomeWork7.studentdata.student.CreateNewStudent;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class WriteObjectsInFile {
    /***
     *записываем студентов в файл
     * @param linkForFile путь к файлу созданных студентов
     * @throws IOException
     */
    public void writeObjectStudentInFile(String linkForFile, int number) throws IOException {
        FileOutputStream fos = new FileOutputStream(linkForFile);
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        CreateNewStudent student = new CreateNewStudent();
        try {//видосик посмотрел
            for (int i = 0; i < number; i++) {
                oos.writeObject(student.CreateStudent());
            }
        } finally {
            oos.flush();
            oos.close();
        }
    }
}
