package by.academy.HomeWork7.studentdata.student;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Student implements Serializable {

    private String firstName;
    private String lastName;
    private String nick;
    private String password;
    private LocalDate registration;
    private LocalDate dateOfBirth;
    private List<Student> student;


    public Student(String firstName,
                   String lastName,
                   String nick,
                   String password,
                   LocalDate registration,
                   LocalDate dateOfBirth
    ) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.nick = nick;
        this.password = password;
        this.registration = registration;
        this.dateOfBirth = dateOfBirth;
        student = new ArrayList<>();
    }

    public Student() {

    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public LocalDate getRegistration() {
        return registration;
    }

    public void setRegistration(LocalDate registration) {
        this.registration = registration;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public void setStudent(List<Student> student) {
        this.student = student;
    }

    public String toString() {
        return "Будущий выдающеся студент нашего прекрасного Института "
                +(char) 27 + "[4m" + (char) 27 + "[7m" + (char) 27 + "[32m" + lastName + (char) 27 + "[0m" + "  "
                + (char) 27 + "[4m" + (char) 27 + "[7m" + (char) 27 + "[31m" + firstName + (char) 27 + "[0m" + "  "
                + " дата рождения  " + (char) 27 + "[4m" + (char) 27 + "[7m" + (char) 27 + "[33m" + dateOfBirth
                + (char) 27 + "[0m" + "  "
                 + " логин " + nick + "  "
                + " дата регистрации" + registration + "  "
                + " пароль: " + password + "  ";

    }

    public List<Student> getStudent() {
        return student;
    }
}
