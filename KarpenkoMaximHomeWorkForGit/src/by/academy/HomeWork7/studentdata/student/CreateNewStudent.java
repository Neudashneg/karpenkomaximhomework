package by.academy.HomeWork7.studentdata.student;

import by.academy.HomeWork4.datacontainer.DataForGenerator;

public class CreateNewStudent {
    /***
     * Создаем студента у которого есть имя, фамилия, логин, пароль,
     * дата регистрации, дата рождения.
     * для создания используем генератор из homework4.dataforgenerator дописав необходимые методы
     * а так же конструктор HomeWork7.studentdata.student
     * @return
     */
    public Student CreateStudent() {
        DataForGenerator dataForStudent = new DataForGenerator();
        return new Student(
                dataForStudent.setFirstName(),
                dataForStudent.setLastName(),
                dataForStudent.setNick() ,
                dataForStudent.setPassword(),
                dataForStudent.setPersonDataRegistration(),
                dataForStudent.setDateOfBirth());
    }
}
