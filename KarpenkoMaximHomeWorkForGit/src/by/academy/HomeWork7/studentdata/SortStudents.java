package by.academy.HomeWork7.studentdata;

import by.academy.HomeWork7.studentdata.student.Student;
import by.academy.HomeWork7.studentdata.comparator.ComporatorDateOfBirth;
import by.academy.HomeWork7.studentdata.comparator.ComporatorFirstName;
import by.academy.HomeWork7.studentdata.comparator.ComporatorLastName;

import java.util.Comparator;
import java.util.List;

public class SortStudents {

    /***
     * сортируем стуентов по позырьковому методу при помощи цепочки компораторов
     * @param students список не сортированных студентов
     * @return возвращаем список сортированных студентов
     */
    public List<Student> sort(List<Student> students) {
        //первоночально сравниваем по фамилии, затем по имени, затем по дате рождения
        Comparator<Student> comparator = new ComporatorLastName().thenComparing(
                new ComporatorFirstName().thenComparing(
                        new ComporatorDateOfBirth()));
        for (int j = 0; j < students.size(); j++) {
            for (int i = 0; i < students.size() - 1; i++) {
                if (comparator.compare(students.get(j), students.get(i)) < 0) {
                    shiftingElements(i, j, students);
                }
            }
        }
        return students;
    }

    /***
     * меняем местмами два элементы списка студентов
     * @param i второй элемент списка студентов
     * @param j первый элемент списка студентов
     * @param students список студентво в котором меняем элементы местами
     */
    private void shiftingElements(int i, int j, List<Student> students) {
        Student tempStudent;
        tempStudent = students.get(j);
        students.set(j, students.get(i));
        students.set(i, tempStudent);
    }
}
