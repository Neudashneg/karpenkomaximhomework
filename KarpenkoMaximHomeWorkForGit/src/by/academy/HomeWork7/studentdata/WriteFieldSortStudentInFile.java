package by.academy.HomeWork7.studentdata;

import by.academy.HomeWork7.studentdata.student.Student;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.List;

public class WriteFieldSortStudentInFile {

    /***
     * Сохраняем в виде байтов поля отсортированных студентов в файл
     * @param students список от сортированных студентов
     * @throws IOException
     */
    public void writeSortedFieldsInFile(List<Student> students,String linkForSortFile) throws IOException {
        FileOutputStream fos = new FileOutputStream(linkForSortFile);
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        try {
            for (Student s : students) {
                oos.writeBytes(s.getFirstName());
                oos.writeBytes(s.getLastName());
                oos.writeBytes(s.getNick());
                oos.writeBytes(s.getPassword());
                int strYear = s.getRegistration().getYear();
                int strMonth = s.getRegistration().getMonthValue();
                int strDays = s.getRegistration().getYear();
                String sirRegistration = strYear + "." + strMonth + "." + strDays;
                oos.writeBytes(sirRegistration);
                int strYearDate = s.getRegistration().getYear();
                int strMonthDate = s.getRegistration().getMonthValue();
                int strDaysDate = s.getRegistration().getYear();
                String strDateOfBirth = strYearDate + "." + strMonthDate + "." + strDaysDate;
                oos.writeBytes(strDateOfBirth);
            }
        } finally {
            oos.flush();
            oos.close();
        }
    }


}
