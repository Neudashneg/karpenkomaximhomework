package by.academy.HomeWork7.studentdata;

import by.academy.HomeWork7.studentdata.student.Student;

import java.util.ArrayList;
import java.util.List;

public class StudentData {
    List<Student> studentList;

    public StudentData() {
        this.studentList = new ArrayList<>();
    }

    public List<Student> getStudentList() {
        return studentList;
    }


}
