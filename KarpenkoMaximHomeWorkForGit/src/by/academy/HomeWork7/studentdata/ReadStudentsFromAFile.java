package by.academy.HomeWork7.studentdata;

import by.academy.HomeWork7.studentdata.student.Student;

import java.io.EOFException;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.List;

public class ReadStudentsFromAFile {
    /***
     * читаем из файла студентов и добавляем в список
     * @return возвращаем список студентов прочитанных из файла
     * @throws IOException
     */
    public List<Student> readStudentFromFile(String linkForFile) throws IOException {
        StudentData studentData = new StudentData();
        FileInputStream fis = new FileInputStream(linkForFile);
        ObjectInputStream ois = new ObjectInputStream(fis);
        try {
            while (true) {
                try {
                    studentData.getStudentList().add((Student) (ois.readObject()));
                } catch (EOFException | ClassNotFoundException e) {
                    break;
                }
            }
        } finally {
            ois.close();
        }
        return studentData.getStudentList();
    }
}
