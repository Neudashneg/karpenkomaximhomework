package by.academy.HomeWork7.studentdata.comparator;

import by.academy.HomeWork7.studentdata.student.Student;

import java.util.Comparator;

public class ComporatorDateOfBirth implements Comparator<Student> {
    @Override
    public int compare(Student o1, Student o2) {
        return o1.getDateOfBirth().compareTo(o2.getDateOfBirth());
    }
}