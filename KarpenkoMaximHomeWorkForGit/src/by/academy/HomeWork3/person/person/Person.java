package by.academy.HomeWork3.person.person;

import by.academy.HomeWork3.person.passengers.Passengers;
import by.academy.HomeWork3.person.person.garage.OpenOrCloseGarage;
import by.academy.HomeWork3.person.person.garage.car.StatusOfCar;
import by.academy.HomeWork3.person.person.garage.car.ConditionsAndActionsWithTheCar;
import by.academy.HomeWork3.person.person.garage.car.TechnicalCharacteristicsOfTheCar;
import by.academy.HomeWork3.person.person.garage.car.key.KeyForCar;

public class Person implements StatusOfCar, ConditionsAndActionsWithTheCar, TechnicalCharacteristicsOfTheCar, OpenOrCloseGarage {
    private Integer numberPhoneForPerson;
    private String firstNamePerson;
    private String secondNamePerson;
    private String uniqueIdKeyForGarageP;
    private String uniqueIdKeyForCarP;
    private String nameOfFuelForRefil;
    private Double howMuchFuelDoWeWantToFillUp;
    private String gpsDestinationData;

    public String getUniqueIdKeyForGarageP() {
        return uniqueIdKeyForGarageP;
    }

    public void setUniqueIdKeyForGarageP(String uniqueIdKeyForGarageP) {
        this.uniqueIdKeyForGarageP = uniqueIdKeyForGarageP;
    }

    public String getGpsDestinationData() {
        return gpsDestinationData;
    }

    public void setGpsDestinationData(String gpsDestinationData) {
        this.gpsDestinationData = gpsDestinationData;
    }

    public Double getHowMuchFuelDoWeWantToFillUp() {
        return howMuchFuelDoWeWantToFillUp;
    }

    public void setHowMuchFuelDoWeWantToFillUp(Double howMuchFuelDoWeWantToFillUp) {
        this.howMuchFuelDoWeWantToFillUp = howMuchFuelDoWeWantToFillUp;
    }

    public String getNameOfFuelForRefil() {
        return nameOfFuelForRefil;
    }


    public void setNameOfFuelForRefil(String nameOfFuelForRefil) {
        this.nameOfFuelForRefil = nameOfFuelForRefil;
    }

    public Boolean insertTheFuelHose() {
        if (openedTheFuelTankCover()) {
            System.out.println("Топлинвый шланг вставлен");
            return true;
        } else {
            System.out.println("Невозможно встаить шалнгБ топливный бак закрыт");
            return false;
        }

    }


    public Boolean removeTheFuelHose() {
        if (insertTheFuelHose() && 0D != refuelTheTank(getNameOfFuelForRefil(), getHowMuchFuelDoWeWantToFillUp())) {
            System.out.println("Топлинвый шланг вынут");
            return true;
        } else {
            System.out.println("Топлинвый шланг не был вставлен");
            return false;
        }
    }


    public Boolean passengerTransport(Integer numberSeatsInCar, Integer numberOfEmptySeatsInCar) {//потаксуем
        return numberSeatsInCar.equals(numberOfEmptySeatsInCar);
    }


    public Integer numberOfEmptySeatsInCar(Integer numberSeatsInCar, Integer numberOfPassengerInTheCar) {
        return numberSeatsInCar - (1 + numberOfPassengerInTheCar);
    }

    /**
     * заглушить машину
     * вставить ключ
     * проверить ключ + сигнализация
     * открыть топливный бак
     * вставить топливный шланг
     * заправить
     * вынуть тполивный шланг
     * вставить ключ
     * закрыть топливный бак
     */


    public void refuelingTheCar() {//заправка машины
        stoppedEngine();
        removeTheKeyIntoTheLock();
        openedTheCarDoor(true);
        closedTheCarDoor(false);
        openedTheFuelTankCover();
        insertTheFuelHose();
        refuelTheTank(getNameOfFuelForRefil(), getHowMuchFuelDoWeWantToFillUp());
        removeTheFuelHose();
        closedTheFuelTankCover();
    }


    /**
     * Езда на машине
     * открыть гараж
     * вставить ключ
     * открыть дверь машины
     * вынуть ключ
     * сесть в машину
     * закрыть дверь машины
     * вставить ключ в замок зажигания
     * завести машину
     * выехать из гаража==ехать на машине
     * заглушить
     * вынуть ключ
     * выйти из машины
     * закрыть дверь
     * закрыть дверь гаража на ключ
     */

    public void leavingTheGarage() {
        openGarage();
        goByCar(getGpsDestinationData());
        stoppedEngine();
        removeTheKeyIntoTheLock();
        closedTheCarDoor(false);
        closeGarage();
    }


    public Boolean openGarage() {
        if (getStatusGarage()) {
            if (getStatusTheLockInGarageIsClosed()) {
                System.out.println("Ключ вставлен в замок двери гаража");
                ;
                openOrCloseDoor(uniqueIdKeyForGarageP);
                System.out.println("Ключ вынут из замка двери гаража");
            } else {
                System.out.println("Замок гаража открыт");
                return true;
            }
            System.out.println("Гараж открыт");
            return true;
        } else {
            System.out.println("Гараж уже открыт");
            return true;
        }
    }


    public Boolean closeGarage() {
        if (getStatusGarage() == false) {
            if (getStatusTheLockInGarageIsClosed() == false) {
                System.out.println("Ключ вставлен в замок двери гаража");
                ;
                openOrCloseDoor(uniqueIdKeyForGarageP);
                System.out.println("Ключ вынут из замка двери гаража");
            } else {
                System.out.println("Замок гаража закрыт");
                return true;
            }
            System.out.println("Гараж закрыт");
            return true;
        } else {
            System.out.println("Гараж уже заткрыт");
            return true;
        }
    }


    public void goByCar(String gpsDestinationData) {
        insertTheKeyIntoTheLock();
        openedTheCarDoor(false);
        removeTheKeyIntoTheLock();
        getInTheCar();
        closedTheCarDoor(true);
        insertTheKeyIntoTheLock();
        firstStepToStarTheCart(getUniqueIdKeyForCarP());
        while (!getGpsData().equals(gpsDestinationData)) {
            System.out.println("Я еду! берегись");
        }
        stopped();
        stoppedEngine();
        removeTheKeyIntoTheLock();
        leaveTheCar();
    }


    public void getInTheCar() {
        System.out.println(" Сел в машину");
    }

    public void getInTheCarPassenger() {
        if (getNumberOfEmptySeatsInCar() > 0) {
            System.out.println(" Сел в машину");
        } else {
            System.out.println("В машине нет мест!");
        }
    }

    public void leaveTheCar() {
        System.out.println("Вышел из машины");
    }

    public void leaveTheCarPassenger() {
        System.out.println("Вышел из машины");
    }


    public void insertTheKeyIntoTheLock() {
        //обобщенно: замок зажигания машины, замок от бензабака машины, замок от дверей машины
        KeyForCar keyForCar = new KeyForCar();
        if (!keyForCar.getStatusInsertAKey()) {
            keyForCar.setStatusInsertAKey(true);
            System.out.println("Ключ вставлен в замок");
        } else {
            System.out.println("Ключ уже вставлен в замок");
        }

    }

    public void removeTheKeyIntoTheLock() {
        //обобщенно: замок зажигания машины, замок от бензабака машины, замок от дверей машины
        KeyForCar keyForCar = new KeyForCar();
        if (keyForCar.getStatusInsertAKey()) {
            keyForCar.setStatusInsertAKey(false);
            System.out.println("Ключ вытащен из замка");
        } else {
            System.out.println("Ключ уже вытащен из замка");
        }
    }

    @Override
    public Boolean firstStepToStarTheCart(String uniqueIdKeyForCar) {
        return null;
    }

    @Override
    public void closedTheFuelTankCover() {

    }

    @Override
    public Boolean openedTheCarDoor(Boolean theDoorLockOpensFromTheInside) {
        return null;
    }

    @Override
    public Boolean closedTheCarDoor(Boolean theDoorLockClosesFromTheInside) {
        return null;
    }

    @Override
    public Double refuelTheTank(String nameOfFuel, Double howMuchFuelDoWeWantToFillUp) {
        return null;
    }

    @Override
    public Boolean openedTheFuelTankCover() {
        return null;
    }

    @Override
    public void stoppedEngine() {

    }

    @Override
    public Boolean stalledTheEngine(Boolean a) {
        return null;
    }

    /**
     * потаксуем
     * остановиться
     * взять пассажира если есть места
     * пассажир садиться в машину
     * отвезти по назначению
     * взять деньжку
     * выпустить на волю
     */


    public void goTaxi() {//потаксуем
        Passengers passengers = new Passengers("Bonopasrt","Napaleon",8749542);
        if (getNumberOfEmptySeatsInCar() > 0 && passengers.getAreThereAnyPassengers()) {
            stopped();
            passengersSitInCar();
            goByCar(passengers.getGpsData());
            System.out.println("МЫ ПРИЕХАЛИ! ДЕНЬГИ ИЛИ ЖИЗНЬ!");
            passengersLeaveCar();

        } else {
            System.out.println("Нет мест или голосующих");
        }
    }

    public void passengersSitInCar() {
        openedTheCarDoor(true);
        System.out.println("Пассажир сел в машину");
        closedTheCarDoor(true);
    }
    public void passengersLeaveCar() {
        openedTheCarDoor(true);
        System.out.println("Пассажир вышел из машины");
        closedTheCarDoor(true);
    }


    public void stopped() {
        System.out.println("Машина останавилась");
    }


    public Integer getNumberPhoneForPerson() {
        return numberPhoneForPerson;
    }

    public void setNumberPhoneForPerson(Integer numberPhoneForPerson) {
        this.numberPhoneForPerson = numberPhoneForPerson;
    }

    public String getFirstNamePerson() {
        return firstNamePerson;
    }

    public void setFirstNamePerson(String firstNamePerson) {
        this.firstNamePerson = firstNamePerson;
    }

    public String getSecondNamePerson() {
        return secondNamePerson;
    }

    public void setSecondNamePerson(String secondNamePerson) {
        this.secondNamePerson = secondNamePerson;
    }

    public String getUniqueIdKeyForCarP() {
        return uniqueIdKeyForCarP;
    }

    public void setUniqueIdKeyForCarP(String uniqueIdKeyForCarP) {
        this.uniqueIdKeyForCarP = uniqueIdKeyForCarP;
    }

    @Override
    public Boolean openOrCloseDoor(String uniqueIdKeyForGarage) {
        return null;
    }

    @Override
    public Boolean getStatusTheLockInGarageIsClosed() {
        return null;
    }

    @Override
    public void setStatusTheLockInGarageIsClosed(Boolean statusTheLockInGarageIsClosed) {

    }

    @Override
    public Boolean getStatusGarage() {
        return null;
    }

    @Override
    public void setStatusGarage(Boolean statusGarage) {

    }

    @Override
    public Boolean getStatusToStartEngine() {
        return null;
    }

    @Override
    public void setStatusToStartEngine(Boolean statusToStartEngine) {

    }

    @Override
    public void getStatusToStartEngine(Boolean statusToStartEngine) {

    }

    @Override
    public Boolean checkStatusForWheel(double statusForWheel) {
        return null;
    }

    @Override
    public Boolean checkTypeEngineAndTypeFuel(Integer numberOfEngineFuel, Integer numberOfEngineType) {
        return null;
    }

    @Override
    public String getGpsData() {
        return null;
    }

    @Override
    public void setGpsData(String gpsData) {

    }

    @Override
    public Boolean getStatusTheDoorIsClosed() {
        return null;
    }

    @Override
    public Boolean getStatusTheLockInDoorIsClosed() {
        return null;
    }

    @Override
    public void setStatusTheLockInDoorIsClosed(Boolean statusTheLockInDoorIsClosed) {

    }

    @Override
    public void setStatusTheDoorIsClosed(Boolean statusTheDoorIsClosed) {

    }

    @Override
    public Integer getNumberSeatsInCar() {
        return null;
    }

    @Override
    public String getTheBrandOfTheMachine() {
        return null;
    }

    @Override
    public String getMachineModel() {
        return null;
    }

    @Override
    public Integer getRegistrationNumberOfTheCar() {
        return null;
    }

    @Override
    public Integer setWheelDiameter() {
        return null;
    }

    @Override
    public Integer setNumberWheelsOfCar() {
        return null;
    }

    @Override
    public Double setMaxLevelFuelInTheTank() {
        return null;
    }

    @Override
    public Integer setNumberOfDoorsInCar() {
        return null;
    }

    @Override
    public Double setMaxPowerEngine() {
        return null;
    }

    @Override
    public String setFuelTypeForEngine(Integer numberOfEngineType) {
        return null;
    }

    @Override
    public String setFuelForEngine(Integer numberOfEngineType) {
        return null;
    }

    @Override
    public Integer setNumberSeatsInCar() {
        return null;
    }

    @Override
    public String setTheBrandOfTheMachine() {
        return null;
    }

    @Override
    public String setMachineModel() {
        return null;
    }

    @Override
    public Integer setRegistrationNumberOfTheCar() {
        return null;
    }

    @Override
    public void setNumberSeatsInCar(Integer numberSeatsInCar) {

    }

    @Override
    public Integer getNumberOfEmptySeatsInCar() {
        return null;
    }

    @Override
    public void setNumberOfEmptySeatsInCar(Integer numberOfEmptySeatsInCar) {

    }

    @Override
    public void setMaxLevelFuelInTheTank(Double maxLevelFuelInTheTank) {

    }

    @Override
    public Double getFuelLevelInTheTank() {
        return null;
    }

    @Override
    public void setFuelLevelInTheTank(Double fuelLevelInTheTank) {

    }

    @Override
    public Boolean noFuelInTheTank(Double fuelLevelInTheTank) {
        return null;
    }

    @Override
    public void lowLevelFuelInTheTank() {

    }

    @Override
    public Boolean runningOutOfFuel(Double fuelLevelInTheTank) {
        return null;
    }

    @Override
    public Integer getNumberOfDoorsInCar() {
        return null;
    }

    @Override
    public void setNumberOfDoorsInCar(Integer numberOfDoorsInCar) {

    }

    @Override
    public Integer getNumberOfEngineFuel() {
        return null;
    }

    @Override
    public Integer getWheelDiameter() {
        return null;
    }

    @Override
    public Integer getNumberWheelsOfCar() {
        return null;
    }

    @Override
    public Double getMaxLevelFuelInTheTank() {
        return null;
    }

    @Override
    public Double getMaxPowerEngine() {
        return null;
    }

    @Override
    public String getFuelTypeForEngine(Integer numberOfEngineType) {
        return null;
    }

    @Override
    public String getFuelForEngine(Integer numberOfEngineType) {
        return null;
    }

    @Override
    public Boolean openOrCloseLockForCar(String uniqueIdKeyForCar) {
        return null;
    }
}
