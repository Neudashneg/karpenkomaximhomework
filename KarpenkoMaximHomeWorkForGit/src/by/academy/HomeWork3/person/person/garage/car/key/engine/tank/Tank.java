package by.academy.HomeWork3.person.person.garage.car.key.engine.tank;

import by.academy.HomeWork3.person.person.garage.car.StatusOfCar;
import by.academy.HomeWork3.person.person.garage.car.TechnicalCharacteristicsOfTheCar;

public class Tank implements  TechnicalCharacteristicsOfTheCar, StatusOfCar {
    private Double maxLevelFuelInTheTank;
    private Double fuelLevelInTheTank;

    @Override
    public Boolean getStatusToStartEngine() {
        return null;
    }

    @Override
    public void setStatusToStartEngine(Boolean statusToStartEngine) {

    }

    @Override
    public void getStatusToStartEngine(Boolean statusToStartEngine) {

    }

    @Override
    public Boolean checkStatusForWheel(double statusForWheel) {
        return null;
    }

    @Override
    public Boolean checkTypeEngineAndTypeFuel(Integer numberOfEngineFuel, Integer numberOfEngineType) {
        return null;
    }

    @Override
    public String getGpsData() {
        return null;
    }

    @Override
    public void setGpsData(String gpsData) {

    }

    @Override
    public Boolean getStatusTheDoorIsClosed() {
        return null;
    }

    @Override
    public Boolean getStatusTheLockInDoorIsClosed() {
        return null;
    }

    @Override
    public void setStatusTheLockInDoorIsClosed(Boolean statusTheLockInDoorIsClosed) {

    }

    @Override
    public void setStatusTheDoorIsClosed(Boolean statusTheDoorIsClosed) {

    }

    @Override
    public void setNumberSeatsInCar(Integer numberSeatsInCar) {

    }

    @Override
    public Integer getNumberOfEmptySeatsInCar() {
        return null;
    }

    @Override
    public void setNumberOfEmptySeatsInCar(Integer numberOfEmptySeatsInCar) {

    }

    public void setMaxLevelFuelInTheTank(Double maxLevelFuelInTheTank) {
        this.maxLevelFuelInTheTank = maxLevelFuelInTheTank;
    }

    public Double getFuelLevelInTheTank() {
        return fuelLevelInTheTank;
    }

    public void setFuelLevelInTheTank(Double fuelLevelInTheTank) {
        this.fuelLevelInTheTank = fuelLevelInTheTank;
    }

    public Boolean runningOutOfFuel(Double fuelLevelInTheTank) {//fuel < 10%
        if (fuelLevelInTheTank < 10*this.maxLevelFuelInTheTank/100) {
            lowLevelFuelInTheTank();
            return true;
        } else {
            return false;
        }
    }



    public Boolean noFuelInTheTank(Double fuelLevelInTheTank) {//fuel < 1%
        if (fuelLevelInTheTank < 1*this.maxLevelFuelInTheTank/100) {
            lowLevelFuelInTheTank();
            return true;
        } else {
            return false;
        }
    }



    public void lowLevelFuelInTheTank(){
        System.out.println("Need fuel, in tank "+(this.maxLevelFuelInTheTank/100)+" %!");
    }

    @Override
    public Integer getNumberOfDoorsInCar() {
        return null;
    }

    @Override
    public void setNumberOfDoorsInCar(Integer numberOfDoorsInCar) {

    }

    @Override
    public Integer getNumberOfEngineFuel() {
        return null;
    }

    @Override
    public Integer getWheelDiameter() {
        return null;
    }

    @Override
    public Integer getNumberWheelsOfCar() {
        return null;
    }

    public Double getMaxLevelFuelInTheTank() {
        return maxLevelFuelInTheTank;
    }

    @Override
    public Double getMaxPowerEngine() {
        return null;
    }

    @Override
    public String getFuelTypeForEngine(Integer numberOfEngineType) {
        return null;
    }

    @Override
    public String getFuelForEngine(Integer numberOfEngineType) {
        return null;
    }

    @Override
    public Integer getNumberSeatsInCar() {
        return null;
    }

    @Override
    public String getTheBrandOfTheMachine() {
        return null;
    }

    @Override
    public String getMachineModel() {
        return null;
    }

    @Override
    public Integer getRegistrationNumberOfTheCar() {
        return null;
    }

    @Override
    public Integer setWheelDiameter() {
        return null;
    }

    @Override
    public Integer setNumberWheelsOfCar() {
        return null;
    }

    @Override
    public Double setMaxLevelFuelInTheTank() {
        return null;
    }

    @Override
    public Integer setNumberOfDoorsInCar() {
        return null;
    }

    @Override
    public Double setMaxPowerEngine() {
        return null;
    }

    @Override
    public String setFuelTypeForEngine(Integer numberOfEngineType) {
        return null;
    }

    @Override
    public String setFuelForEngine(Integer numberOfEngineType) {
        return null;
    }

    @Override
    public Integer setNumberSeatsInCar() {
        return null;
    }

    @Override
    public String setTheBrandOfTheMachine() {
        return null;
    }

    @Override
    public String setMachineModel() {
        return null;
    }

    @Override
    public Integer setRegistrationNumberOfTheCar() {
        return null;
    }

    @Override
    public Boolean openOrCloseLockForCar(String uniqueIdKeyForCar) {
        return null;
    }


}

