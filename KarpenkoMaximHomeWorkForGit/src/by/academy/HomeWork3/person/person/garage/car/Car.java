package by.academy.HomeWork3.person.person.garage.car;

import by.academy.HomeWork1.task2.calculatorwithoperator.ICalculator;
import by.academy.HomeWork3.person.person.Person;
import by.academy.HomeWork3.person.person.garage.car.key.KeyForCar;

public class Car implements ConditionsAndActionsWithTheCar, StatusOfCar, TechnicalCharacteristicsOfTheCar {
    private String theBrandOfTheMachine;
    private String machineModel;
    private Integer registrationNumberOfTheCar;
    private String gpsData;

    public Car() {

    }


    public void stoppedEngine() {
        if (getStatusToStartEngine()) {
            setStatusToStartEngine(false);
            System.out.println(" Двигатель заглушен");
        } else {
            System.out.println("Двигатель не был заведен");
        }
    }


    public Boolean stalledTheEngine(Boolean a) {//заглохла//a условия для "заглохла"
        if (getStatusToStartEngine()) {
            if (a) {
                setStatusToStartEngine(false);
                System.out.println("Check Car, you don't driving in this car");
                return true;
            } else {
                System.out.println("Bi bi bi I CRAZY CAR");
                return false;
            }
        } else {
            System.out.println("Как я заглохну, если я даже не заведенa, LOL");
            return false;
        }
    }


    @Override
    public Double getHowMuchFuelDoWeWantToFillUp() {
        return null;
    }

    @Override
    public String getUniqueIdKeyForCarP() {
        return null;
    }

    @Override
    public String getNameOfFuelForRefil() {
        return null;
    }

    @Override
    public Boolean insertTheFuelHose() {
        return null;
    }

    @Override
    public Boolean removeTheFuelHose() {
        return null;
    }

    @Override
    public void insertTheKeyIntoTheLock() {

    }

    @Override
    public void removeTheKeyIntoTheLock() {

    }

    @Override
    public Boolean openOrCloseLockForCar(String uniqueIdKeyForCar) {
        return null;
    }

    public Boolean firstStepToStarTheCart(String uniqueIdKeyForCar) {
        Person person = new Person();
        KeyForCar keyForCar = new KeyForCar();
        if (uniqueIdKeyForCar.equals(keyForCar.getUniqueIdKeyForCar())) {
            return true;
        } else {
            System.out.println(" Call 911" + " I Car! " + getTheBrandOfTheMachine() + getMachineModel()
                    + getRegistrationNumberOfTheCar() + " they're trying to wind me up with the wrong key "
                    + getGpsData() + " my owner's name " + person.getFirstNamePerson()
                    + person.getSecondNamePerson() + " my owner's phone number " + person.getNumberPhoneForPerson());
            return false;
        }
    }


    public Boolean openedTheFuelTankCover() {
        insertTheKeyIntoTheLock();//lockincarforcar
        if (openOrCloseLockForCar(getUniqueIdKeyForCarP())) {
            System.out.println("Топливный бак открыт");
            removeTheKeyIntoTheLock();//lockincarforcar
            return true;
        } else {
            System.out.println("Замок топливного бака закрыт");
            removeTheKeyIntoTheLock();//lockincarforcar
            return false;
        }

    }


    public void closedTheFuelTankCover() {
        if (removeTheFuelHose()) {
            insertTheKeyIntoTheLock();//lockincarforcar
            System.out.println("Топливный бак закрыт");
            removeTheKeyIntoTheLock();//lockincarforcar
        } else {
            System.out.println("Топливный шланг не вынут");
        }
    }

    public Boolean openedTheCarDoor(Boolean theDoorLockOpensFromTheInside) {
        if (getStatusTheDoorIsClosed()) {
            if (getStatusTheLockInDoorIsClosed()) {
                if (!theDoorLockOpensFromTheInside) {
                    insertTheKeyIntoTheLock();//lockincarforcar
                    if (openOrCloseLockForCar(getUniqueIdKeyForCarP())) {
                        System.out.println("Замок двери открыт");
                        removeTheKeyIntoTheLock();//lockincarforcar
                        return true;
                    } else {
                        System.out.println("Замок двери без изменения");
                        removeTheKeyIntoTheLock();//lockincarforcar
                        return false;
                    }
                } else {  //изнутри машины замок условно открываеться автоматически при открытии двери
                    System.out.println("Замок двери открыт");
                    return true;
                }
            } else {
                System.out.println("Замок двери уже открыт");
                return true;
            }
        } else {
            System.out.println("Дверь уже открыта");
            return true;
        }
    }


    public Boolean closedTheCarDoor(Boolean theDoorLockClosesFromTheInside) {
        if (!getStatusTheDoorIsClosed()) {
            if (!getStatusTheLockInDoorIsClosed()) {
                if (!theDoorLockClosesFromTheInside) {
                    insertTheKeyIntoTheLock();//lockincarforcar
                    if (openOrCloseLockForCar(getUniqueIdKeyForCarP())) {
                        System.out.println("Замок двери закрыт");
                        removeTheKeyIntoTheLock();//lockincarforcar
                        return true;
                    } else {
                        System.out.println("Замок двери без изменения");
                        removeTheKeyIntoTheLock();//lockincarforcar
                        return false;
                    }
                } else {//изнутри машины замок условно открываеться автоматически при открытии двери
                    // также и при закрытии изнутри
                    System.out.println("Замок двери закрыт");
                    return true;
                }
            } else {
                System.out.println("Замок двери уже закрыт");
                return true;
            }
        } else {
            System.out.println("Дверь уже закрыта");
            return true;
        }
    }

    @Override
    public Double refuelTheTank(String nameOfFuel, Double howMuchFuelDoWeWantToFillUp) {
        return null;
    }


    public Double refuelTheTank(String nameOfFuel, double howMuchFuelDoWeWantToFillUp) {//смысл от double если актуально void
        if (insertTheFuelHose()) {
            if (nameOfFuel.equals(this.getFuelTypeForEngine(this.getNumberOfEngineFuel()))) {
                double firstFuelLevelInTheTank = getFuelLevelInTheTank();
                while (getFuelLevelInTheTank() < getMaxLevelFuelInTheTank()) {
                    setFuelLevelInTheTank(getFuelLevelInTheTank() + 0.1D);
                    if (getFuelLevelInTheTank().equals(getMaxLevelFuelInTheTank())) {
                        break;
                    } else if (getFuelLevelInTheTank() == howMuchFuelDoWeWantToFillUp + firstFuelLevelInTheTank) {
                        break;
                    }
                }
            }
        } else {
            System.out.println("Топливный шланг не вставлен");
        }
        return getFuelLevelInTheTank();
    }


    public String getTheBrandOfTheMachine() {
        return theBrandOfTheMachine;
    }

    public void setTheBrandOfTheMachine(String theBrandOfTheMachine) {
        this.theBrandOfTheMachine = theBrandOfTheMachine;
    }

    public String getMachineModel() {
        return machineModel;
    }

    public void setMachineModel(String machineModel) {
        this.machineModel = machineModel;
    }

    public Integer getRegistrationNumberOfTheCar() {
        return registrationNumberOfTheCar;
    }
/*
    @Override
    public Integer setWheelDiameter() {
        return null;
    }

    @Override
    public Integer setNumberWheelsOfCar() {
        return null;
    }

    @Override
    public Double setMaxLevelFuelInTheTank() {
        return null;
    }

    @Override
    public Integer setNumberOfDoorsInCar() {
        return null;
    }

    @Override
    public Double setMaxPowerEngine() {
        return null;
    }

    @Override
    public String setFuelTypeForEngine(Integer numberOfEngineType) {
        return null;
    }

    @Override
    public String setFuelForEngine(Integer numberOfEngineType) {
        return null;
    }

    @Override
    public Integer setNumberSeatsInCar() {
        return null;
    }

    @Override
    public String setTheBrandOfTheMachine() {
        return null;
    }

    @Override
    public String setMachineModel() {
        return null;
    }

    @Override
    public Integer setRegistrationNumberOfTheCar() {
        return null;
    }*/

    public void setRegistrationNumberOfTheCar(Integer registrationNumberOfTheCar) {
        this.registrationNumberOfTheCar = registrationNumberOfTheCar;
    }

    @Override
    public Boolean getStatusToStartEngine() {
        return null;
    }

    @Override
    public void setStatusToStartEngine(Boolean statusToStartEngine) {

    }

    @Override
    public void getStatusToStartEngine(Boolean statusToStartEngine) {

    }

    @Override
    public Boolean checkStatusForWheel(double statusForWheel) {
        return null;
    }

    @Override
    public Boolean checkTypeEngineAndTypeFuel(Integer numberOfEngineFuel, Integer numberOfEngineType) {
        return null;
    }

    public String getGpsData() {
        return gpsData;
    }

    public void setGpsData(String gpsData) {
        this.gpsData = gpsData;
    }

    @Override
    public Boolean getStatusTheDoorIsClosed() {
        return null;
    }

    @Override
    public Boolean getStatusTheLockInDoorIsClosed() {
        return null;
    }

    @Override
    public void setStatusTheLockInDoorIsClosed(Boolean statusTheLockInDoorIsClosed) {

    }

    @Override
    public void setStatusTheDoorIsClosed(Boolean statusTheDoorIsClosed) {

    }

    @Override
    public Integer getNumberSeatsInCar() {
        return null;
    }

    @Override
    public void setNumberSeatsInCar(Integer numberSeatsInCar) {

    }

    @Override
    public Integer getNumberOfEmptySeatsInCar() {
        return null;
    }

    @Override
    public void setNumberOfEmptySeatsInCar(Integer numberOfEmptySeatsInCar) {

    }

    @Override
    public void setMaxLevelFuelInTheTank(Double maxLevelFuelInTheTank) {

    }

    @Override
    public Double getFuelLevelInTheTank() {
        return null;
    }

    @Override
    public void setFuelLevelInTheTank(Double fuelLevelInTheTank) {

    }

    @Override
    public Boolean noFuelInTheTank(Double fuelLevelInTheTank) {
        return null;
    }

    @Override
    public void lowLevelFuelInTheTank() {

    }

    @Override
    public Boolean runningOutOfFuel(Double fuelLevelInTheTank) {
        return null;
    }

    @Override
    public Integer getNumberOfDoorsInCar() {
        return null;
    }

    @Override
    public void setNumberOfDoorsInCar(Integer numberOfDoorsInCar) {

    }

    @Override
    public Integer getNumberOfEngineFuel() {
        return null;
    }

    @Override
    public Integer getWheelDiameter() {
        return null;
    }

    @Override
    public Integer getNumberWheelsOfCar() {
        return null;
    }

    @Override
    public Double getMaxLevelFuelInTheTank() {
        return null;
    }

    @Override
    public Double getMaxPowerEngine() {
        return null;
    }

    @Override
    public String getFuelTypeForEngine(Integer numberOfEngineType) {
        return null;
    }

    @Override
    public String getFuelForEngine(Integer numberOfEngineType) {
        return null;
    }


    private TechnicalCharacteristicsOfTheCar firstCar;

    public Car(TechnicalCharacteristicsOfTheCar firstCar) {
        this.firstCar = firstCar;
    }

    public Integer setWheelDiameter() {
        return firstCar.setWheelDiameter();
    }

    public Integer setNumberWheelsOfCar() {
        return firstCar.setNumberWheelsOfCar();
    }

    public Double setMaxLevelFuelInTheTank() {
        return firstCar.setMaxLevelFuelInTheTank();
    }

    public Integer setNumberOfDoorsInCar() {
        return firstCar.setNumberOfDoorsInCar();
    }

    public Double setMaxPowerEngine() {
        return firstCar.setMaxPowerEngine();
    }

    public String setFuelTypeForEngine(Integer numberOfEngineType) {
        return firstCar.setFuelTypeForEngine(numberOfEngineType);
    }

    public String setFuelForEngine(Integer numberOfEngineType) {
        return firstCar.setFuelForEngine(numberOfEngineType);
    }

    public Integer setNumberSeatsInCar() {
        return firstCar.setNumberSeatsInCar();
    }

    public String setTheBrandOfTheMachine() {
        return firstCar.setTheBrandOfTheMachine();
    }

    public String setMachineModel() {
        return firstCar.setMachineModel();
    }

    public Integer setRegistrationNumberOfTheCar() {
        return firstCar.setRegistrationNumberOfTheCar();
    }

    public TechnicalCharacteristicsOfTheCar getFirstCar() {
        return firstCar;
    }

    public void setFirstCar(TechnicalCharacteristicsOfTheCar firstCar) {
        this.firstCar = firstCar;
    }
}


