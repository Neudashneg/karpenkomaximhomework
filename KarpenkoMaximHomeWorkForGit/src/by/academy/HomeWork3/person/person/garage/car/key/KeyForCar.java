package by.academy.HomeWork3.person.person.garage.car.key;

public class KeyForCar {
    private String uniqueIdKeyForCar;
    private Boolean statusInsertAKey;

    public Boolean getStatusInsertAKey() {
        return statusInsertAKey;
    }

    public void setStatusInsertAKey(Boolean statusInsertAKey) {
        this.statusInsertAKey = statusInsertAKey;
    }

    public String getUniqueIdKeyForCar() {
        return uniqueIdKeyForCar;
    }
}
