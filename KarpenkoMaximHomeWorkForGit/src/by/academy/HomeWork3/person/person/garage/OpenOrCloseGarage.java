package by.academy.HomeWork3.person.person.garage;

public interface OpenOrCloseGarage {
    Boolean openOrCloseDoor(String uniqueIdKeyForGarage);

    public Boolean getStatusTheLockInGarageIsClosed();

    public void setStatusTheLockInGarageIsClosed(Boolean statusTheLockInGarageIsClosed);

    public Boolean getStatusGarage();

    public void setStatusGarage(Boolean statusGarage);
}
