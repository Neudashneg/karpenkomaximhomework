package by.academy.HomeWork3.person.person.garage.car;

public interface TechnicalCharacteristicsOfTheCar {

    Integer getNumberOfDoorsInCar();

    void setNumberOfDoorsInCar(Integer numberOfDoorsInCar);

    Integer getNumberOfEngineFuel();

    Integer getWheelDiameter();

    Integer getNumberWheelsOfCar();

    Double getMaxLevelFuelInTheTank();


    Double getMaxPowerEngine();

    String getFuelTypeForEngine(Integer numberOfEngineType);

    String getFuelForEngine(Integer numberOfEngineType);

    Integer getNumberSeatsInCar();

    String getTheBrandOfTheMachine();

    String getMachineModel();

    Integer getRegistrationNumberOfTheCar();

    Integer setWheelDiameter();

    Integer setNumberWheelsOfCar();

    Double setMaxLevelFuelInTheTank();

    Integer setNumberOfDoorsInCar();

    Double setMaxPowerEngine();

    String setFuelTypeForEngine(Integer numberOfEngineType);

    String setFuelForEngine(Integer numberOfEngineType);

    Integer setNumberSeatsInCar();

    String setTheBrandOfTheMachine();

    String setMachineModel();

    Integer setRegistrationNumberOfTheCar();

}
