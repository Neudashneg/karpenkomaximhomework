package by.academy.HomeWork3.person.person.garage;


import by.academy.HomeWork3.person.person.Person;

public class LockForGarage implements OpenOrCloseGarage {//zamok
    private Boolean statusTheLockInGarageIsClosed;

    public Boolean getStatusTheLockInGarageIsClosed() {
        return statusTheLockInGarageIsClosed;
    }

    public void setStatusTheLockInGarageIsClosed(Boolean statusTheLockInGarageIsClosed) {
        this.statusTheLockInGarageIsClosed = statusTheLockInGarageIsClosed;
    }

    @Override
    public Boolean getStatusGarage() {
        return null;
    }

    @Override
    public void setStatusGarage(Boolean statusGarage) {

    }

    Person person = new Person();
    KeyForGarage keyForGarage = new KeyForGarage();
    Garage garage = new Garage();

    public Boolean openOrCloseDoor(String uniqueIdKeyForGarage) {
        if (uniqueIdKeyForGarage.equals(keyForGarage.getUniqueIdKeyForGarage())) {
            return true;
        } else {
            System.out.println(" Call 911" + "I Garage!" + " they try to open me with the wrong key, my address "
                    + garage.getAddress() + " my owner's name " + person.getFirstNamePerson()
                    + person.getSecondNamePerson() + " my owner's phone number " + person.getNumberPhoneForPerson());
            return false;
        }
    }
}
