package by.academy.HomeWork3.person.person.garage;

import by.academy.HomeWork1.task2.calculatorwithcounter.CalculatorWithCounter;
import by.academy.HomeWork1.task2.calculatorwithoperator.CalculatorWithOperator;
import by.academy.HomeWork1.task2.calculatorwithoperator.ICalculator;
import by.academy.HomeWork3.person.person.garage.car.Car;
import by.academy.HomeWork3.person.person.garage.car.TechnicalCharacteristicsOfTheCar;

public class Garage implements OpenOrCloseGarage {
    private String address;
    private Integer numberOfParkingSpacesInTheGarage;
    private Integer numberCarsInParkingSpacesInTheGarage;
    private Boolean statusGarage;

    @Override
    public Boolean openOrCloseDoor(String uniqueIdKeyForGarage) {
        return null;
    }

    @Override
    public Boolean getStatusTheLockInGarageIsClosed() {
        return null;
    }

    @Override
    public void setStatusTheLockInGarageIsClosed(Boolean statusTheLockInGarageIsClosed) {

    }

    public Boolean getStatusGarage() {
        return statusGarage;
    }

    public void setStatusGarage(Boolean statusGarage) {
        this.statusGarage = statusGarage;
    }

    public String getAddress() {
        return address;
    }
}
