package by.academy.HomeWork3.person.person.garage.car;




public interface ConditionsAndActionsWithTheCar {
    /*person*/
    Double getHowMuchFuelDoWeWantToFillUp();//person

    String getUniqueIdKeyForCarP();//person

    String getNameOfFuelForRefil();//person


    Boolean insertTheFuelHose();//person


    Boolean removeTheFuelHose();//person


    void insertTheKeyIntoTheLock();//person

    void removeTheKeyIntoTheLock();//person


    /*lock*/
    Boolean openOrCloseLockForCar(String uniqueIdKeyForCar);//lock


    /*car*/
    Boolean firstStepToStarTheCart(String uniqueIdKeyForCar);//car

    void closedTheFuelTankCover();//car

    Boolean openedTheCarDoor(Boolean theDoorLockOpensFromTheInside);//car

    Boolean closedTheCarDoor(Boolean theDoorLockClosesFromTheInside);//car

    Double refuelTheTank(String nameOfFuel, Double howMuchFuelDoWeWantToFillUp);//car

    Boolean openedTheFuelTankCover();//car

    //Boolean firstStepToStartTheCar(String uniqueIdKeyForCar);//car

    void stoppedEngine();//car

    Boolean stalledTheEngine(Boolean a);//car


}
