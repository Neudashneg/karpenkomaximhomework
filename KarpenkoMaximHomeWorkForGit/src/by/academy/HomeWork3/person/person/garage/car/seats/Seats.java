package by.academy.HomeWork3.person.person.garage.car.seats;

import by.academy.HomeWork3.person.person.garage.car.StatusOfCar;
import by.academy.HomeWork3.person.person.garage.car.TechnicalCharacteristicsOfTheCar;

public class Seats implements TechnicalCharacteristicsOfTheCar, StatusOfCar {//sidenie
    private Integer numberSeatsInCar;
    private Integer numberOfEmptySeatsInCar;

    @Override
    public Boolean getStatusToStartEngine() {
        return null;
    }

    @Override
    public void setStatusToStartEngine(Boolean statusToStartEngine) {

    }

    @Override
    public void getStatusToStartEngine(Boolean statusToStartEngine) {

    }

    @Override
    public Boolean checkStatusForWheel(double statusForWheel) {
        return null;
    }

    @Override
    public Boolean checkTypeEngineAndTypeFuel(Integer numberOfEngineFuel, Integer numberOfEngineType) {
        return null;
    }

    @Override
    public String getGpsData() {
        return null;
    }

    @Override
    public void setGpsData(String gpsData) {

    }

    @Override
    public Boolean getStatusTheDoorIsClosed() {
        return null;
    }

    @Override
    public Boolean getStatusTheLockInDoorIsClosed() {
        return null;
    }

    @Override
    public void setStatusTheLockInDoorIsClosed(Boolean statusTheLockInDoorIsClosed) {

    }

    @Override
    public void setStatusTheDoorIsClosed(Boolean statusTheDoorIsClosed) {

    }

    @Override
    public Integer getNumberOfDoorsInCar() {
        return null;
    }

    @Override
    public void setNumberOfDoorsInCar(Integer numberOfDoorsInCar) {

    }

    @Override
    public Integer getNumberOfEngineFuel() {
        return null;
    }

    @Override
    public Integer getWheelDiameter() {
        return null;
    }

    @Override
    public Integer getNumberWheelsOfCar() {
        return null;
    }

    @Override
    public Double getMaxLevelFuelInTheTank() {
        return null;
    }

    @Override
    public Double getMaxPowerEngine() {
        return null;
    }

    @Override
    public String getFuelTypeForEngine(Integer numberOfEngineType) {
        return null;
    }

    @Override
    public String getFuelForEngine(Integer numberOfEngineType) {
        return null;
    }

    public Integer getNumberSeatsInCar() {
        return numberSeatsInCar;
    }

    @Override
    public String getTheBrandOfTheMachine() {
        return null;
    }

    @Override
    public String getMachineModel() {
        return null;
    }

    @Override
    public Integer getRegistrationNumberOfTheCar() {
        return null;
    }

    @Override
    public Integer setWheelDiameter() {
        return null;
    }

    @Override
    public Integer setNumberWheelsOfCar() {
        return null;
    }

    @Override
    public Double setMaxLevelFuelInTheTank() {
        return null;
    }

    @Override
    public Integer setNumberOfDoorsInCar() {
        return null;
    }

    @Override
    public Double setMaxPowerEngine() {
        return null;
    }

    @Override
    public String setFuelTypeForEngine(Integer numberOfEngineType) {
        return null;
    }

    @Override
    public String setFuelForEngine(Integer numberOfEngineType) {
        return null;
    }

    @Override
    public Integer setNumberSeatsInCar() {
        return null;
    }

    @Override
    public String setTheBrandOfTheMachine() {
        return null;
    }

    @Override
    public String setMachineModel() {
        return null;
    }

    @Override
    public Integer setRegistrationNumberOfTheCar() {
        return null;
    }

    public void setNumberSeatsInCar(Integer numberSeatsInCar) {
        this.numberSeatsInCar = numberSeatsInCar;
    }

    public Integer getNumberOfEmptySeatsInCar() {
        return numberOfEmptySeatsInCar;
    }

    public void setNumberOfEmptySeatsInCar(Integer numberOfEmptySeatsInCar) {
        this.numberOfEmptySeatsInCar = numberOfEmptySeatsInCar;
    }

    @Override
    public void setMaxLevelFuelInTheTank(Double maxLevelFuelInTheTank) {

    }

    @Override
    public Double getFuelLevelInTheTank() {
        return null;
    }

    @Override
    public void setFuelLevelInTheTank(Double fuelLevelInTheTank) {

    }

    @Override
    public Boolean noFuelInTheTank(Double fuelLevelInTheTank) {
        return null;
    }

    @Override
    public void lowLevelFuelInTheTank() {

    }

    @Override
    public Boolean runningOutOfFuel(Double fuelLevelInTheTank) {
        return null;
    }

    @Override
    public Boolean openOrCloseLockForCar(String uniqueIdKeyForCar) {
        return null;
    }
}
