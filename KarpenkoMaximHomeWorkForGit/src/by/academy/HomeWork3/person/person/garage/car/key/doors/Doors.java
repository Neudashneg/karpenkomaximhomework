package by.academy.HomeWork3.person.person.garage.car.key.doors;

import by.academy.HomeWork3.person.person.garage.car.TechnicalCharacteristicsOfTheCar;

public class Doors implements TechnicalCharacteristicsOfTheCar {

    private Integer numberOfDoorsInCar;
    private Boolean statusTheDoorIsClosed;
    private Boolean statusTheLockInDoorIsClosed;

    public Integer getNumberOfDoorsInCar() {
        return numberOfDoorsInCar;
    }

    public void setNumberOfDoorsInCar(Integer numberOfDoorsInCar) {
        this.numberOfDoorsInCar = numberOfDoorsInCar;
    }

    @Override
    public Integer getNumberOfEngineFuel() {
        return null;
    }

    @Override
    public Integer getWheelDiameter() {
        return null;
    }

    @Override
    public Integer getNumberWheelsOfCar() {
        return null;
    }

    @Override
    public Double getMaxLevelFuelInTheTank() {
        return null;
    }

    @Override
    public Double getMaxPowerEngine() {
        return null;
    }

    @Override
    public String getFuelTypeForEngine(Integer numberOfEngineType) {
        return null;
    }

    @Override
    public String getFuelForEngine(Integer numberOfEngineType) {
        return null;
    }

    @Override
    public Integer getNumberSeatsInCar() {
        return null;
    }

    @Override
    public String getTheBrandOfTheMachine() {
        return null;
    }

    @Override
    public String getMachineModel() {
        return null;
    }

    @Override
    public Integer getRegistrationNumberOfTheCar() {
        return null;
    }

    @Override
    public Integer setWheelDiameter() {
        return null;
    }

    @Override
    public Integer setNumberWheelsOfCar() {
        return null;
    }

    @Override
    public Double setMaxLevelFuelInTheTank() {
        return null;
    }

    @Override
    public Integer setNumberOfDoorsInCar() {
        return null;
    }

    @Override
    public Double setMaxPowerEngine() {
        return null;
    }

    @Override
    public String setFuelTypeForEngine(Integer numberOfEngineType) {
        return null;
    }

    @Override
    public String setFuelForEngine(Integer numberOfEngineType) {
        return null;
    }

    @Override
    public Integer setNumberSeatsInCar() {
        return null;
    }

    @Override
    public String setTheBrandOfTheMachine() {
        return null;
    }

    @Override
    public String setMachineModel() {
        return null;
    }

    @Override
    public Integer setRegistrationNumberOfTheCar() {
        return null;
    }


    public Boolean getStatusTheDoorIsClosed() {
        return statusTheDoorIsClosed;
    }

    public Boolean getStatusTheLockInDoorIsClosed() {
        return statusTheLockInDoorIsClosed;
    }

    public void setStatusTheLockInDoorIsClosed(Boolean statusTheLockInDoorIsClosed) {
        this.statusTheLockInDoorIsClosed = statusTheLockInDoorIsClosed;
    }

    public void setStatusTheDoorIsClosed(Boolean statusTheDoorIsClosed) {
        this.statusTheDoorIsClosed = statusTheDoorIsClosed;
    }

}
