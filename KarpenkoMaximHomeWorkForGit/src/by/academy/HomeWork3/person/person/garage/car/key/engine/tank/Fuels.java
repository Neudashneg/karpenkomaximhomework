package by.academy.HomeWork3.person.person.garage.car.key.engine.tank;

/**
 * Fuel for internal combustion engines
 */
public class Fuels {
    final String FIRST_FUEL = "Benzine";
    final String SECOND_FUEL = "Diesel";
    final String THIRD_FUEL = "Gas";

    public String setTypoFuel(Integer typoFuel) {
        String result;
        switch (typoFuel) {
            case 1:
                result = FIRST_FUEL;
                break;
            case 2:
                result = SECOND_FUEL;
                break;
            case 3:
                result = THIRD_FUEL;
                break;
            case 4:
                result = FIRST_FUEL + THIRD_FUEL;
                break;
            case 5:
                result = SECOND_FUEL + THIRD_FUEL;
                break;
            default:
                result = "The fuel type is set incorrectly";
                break;
        }
        return result;
    }
}
