package by.academy.HomeWork3.person.person.garage.car.key.engine;

import by.academy.HomeWork3.person.person.garage.car.StatusOfCar;
import by.academy.HomeWork3.person.person.garage.car.TechnicalCharacteristicsOfTheCar;
import by.academy.HomeWork3.person.person.garage.car.key.engine.tank.Fuels;
/**
 * Internal combustion engines
 * https://ru.wikipedia.org/wiki/%D0%94%D0%B2%D0%B8%D0%B3%D0%B0%D1%82%D0%B5%D0%BB%D1%8C_%D0%B2%D0%BD%D1%83%D1%82%D1%80%D0%B5%D0%BD%D0%BD%D0%B5%D0%B3%D0%BE_%D1%81%D0%B3%D0%BE%D1%80%D0%B0%D0%BD%D0%B8%D1%8F
 */


public class Engine implements StatusOfCar, TechnicalCharacteristicsOfTheCar {
    private Double maxPowerEngine;
    private String fuelForEngine;
    private String typeForEngine;
    private Integer numberOfEngineFuel;
    private Integer numberOfEngineType;
    private Boolean statusToStartEngine;

    public Boolean getStatusToStartEngine() {
        return statusToStartEngine;
    }

    public void setStatusToStartEngine(Boolean statusToStartEngine) {
        this.statusToStartEngine = statusToStartEngine;
    }

    @Override
    public void getStatusToStartEngine(Boolean statusToStartEngine) {

    }

    @Override
    public Boolean checkStatusForWheel(double statusForWheel) {
        return null;
    }

    public Double getMaxPowerEngine() {
        return maxPowerEngine;
    }

    @Override
    public String getFuelTypeForEngine(Integer numberOfEngineType) {
        return null;
    }

    public void setMaxPowerEngine(Double maxPowerEngine) {
        this.maxPowerEngine = maxPowerEngine;
    }

    public String getFuelForEngine() {
        return fuelForEngine;
    }

    public void setFuelForEngine(String fuelForEngine) {
        this.fuelForEngine = fuelForEngine;
    }

    public String getTypeForEngine() {
        return typeForEngine;
    }

    public void setTypeForEngine(String typeForEngine) {
        this.typeForEngine = typeForEngine;
    }

    @Override
    public Integer getNumberOfDoorsInCar() {
        return null;
    }

    @Override
    public void setNumberOfDoorsInCar(Integer numberOfDoorsInCar) {

    }

    public Integer getNumberOfEngineFuel() {
        return numberOfEngineFuel;
    }

    @Override
    public Integer getWheelDiameter() {
        return null;
    }

    @Override
    public Integer getNumberWheelsOfCar() {
        return null;
    }

    public void setNumberOfEngineFuel(Integer numberOfEngineFuel) {
        this.numberOfEngineFuel = numberOfEngineFuel;
    }

    public Integer getNumberOfEngineType() {
        return numberOfEngineType;
    }

    public void setNumberOfEngineType(Integer numberOfEngineType) {
        this.numberOfEngineType = numberOfEngineType;
    }

    public String getFuelForEngine(Integer numberOfEngineType) {
        Fuels fuels = new Fuels();
        return fuelForEngine = fuels.setTypoFuel(numberOfEngineFuel);
    }


    public Boolean checkTypeEngineAndTypeFuel(Integer numberOfEngineType, Integer numberOfEngineFuel) {//проверка типо двигателя по топливу и топлаива в баке
        return numberOfEngineType.equals(numberOfEngineFuel);

    }

    @Override
    public String getGpsData() {
        return null;
    }

    @Override
    public void setGpsData(String gpsData) {

    }

    @Override
    public Boolean getStatusTheDoorIsClosed() {
        return null;
    }

    @Override
    public Boolean getStatusTheLockInDoorIsClosed() {
        return null;
    }

    @Override
    public void setStatusTheLockInDoorIsClosed(Boolean statusTheLockInDoorIsClosed) {

    }

    @Override
    public void setStatusTheDoorIsClosed(Boolean statusTheDoorIsClosed) {

    }

    @Override
    public Integer getNumberSeatsInCar() {
        return null;
    }

    @Override
    public String getTheBrandOfTheMachine() {
        return null;
    }

    @Override
    public String getMachineModel() {
        return null;
    }

    @Override
    public Integer getRegistrationNumberOfTheCar() {
        return null;
    }

    @Override
    public Integer setWheelDiameter() {
        return null;
    }

    @Override
    public Integer setNumberWheelsOfCar() {
        return null;
    }

    @Override
    public Double setMaxLevelFuelInTheTank() {
        return null;
    }

    @Override
    public Integer setNumberOfDoorsInCar() {
        return null;
    }

    @Override
    public Double setMaxPowerEngine() {
        return null;
    }

    @Override
    public String setFuelTypeForEngine(Integer numberOfEngineType) {
        return null;
    }

    @Override
    public String setFuelForEngine(Integer numberOfEngineType) {
        return null;
    }

    @Override
    public Integer setNumberSeatsInCar() {
        return null;
    }

    @Override
    public String setTheBrandOfTheMachine() {
        return null;
    }

    @Override
    public String setMachineModel() {
        return null;
    }

    @Override
    public Integer setRegistrationNumberOfTheCar() {
        return null;
    }

    @Override
    public void setNumberSeatsInCar(Integer numberSeatsInCar) {

    }

    @Override
    public Integer getNumberOfEmptySeatsInCar() {
        return null;
    }

    @Override
    public void setNumberOfEmptySeatsInCar(Integer numberOfEmptySeatsInCar) {

    }

    @Override
    public void setMaxLevelFuelInTheTank(Double maxLevelFuelInTheTank) {

    }

    @Override
    public Double getFuelLevelInTheTank() {
        return null;
    }

    @Override
    public void setFuelLevelInTheTank(Double fuelLevelInTheTank) {

    }

    @Override
    public Boolean noFuelInTheTank(Double fuelLevelInTheTank) {
        return null;
    }

    @Override
    public void lowLevelFuelInTheTank() {

    }

    @Override
    public Boolean runningOutOfFuel(Double fuelLevelInTheTank) {
        return null;
    }

    @Override
    public Double getMaxLevelFuelInTheTank() {
        return null;
    }

    @Override
    public Boolean openOrCloseLockForCar(String uniqueIdKeyForCar) {
        return null;
    }


}
