package by.academy.HomeWork3.person.person.garage.car.key.engine.typeengine;

public class TypeEngine {
    final String FIRST_TYPE_ENGINE = "Benzine engine";
    final String SECOND_TYPE_ENGINE = "Diesel engine ";
    final String THIRD_TYPE_ENGINE = "Gas engine";
    final String FOURTH_TYPE_ENGINE = "Combination for benzine and gas engine ";
    final String FIFTH_TYPE_ENGINE = "Combination for diesel and gas engine";

    public String setTypoFuel(byte typoFuel) {
        String result;
        switch (typoFuel) {
            case 1:
                result = FIRST_TYPE_ENGINE;
                break;
            case 2:
                result = SECOND_TYPE_ENGINE;
                break;
            case 3:
                result = THIRD_TYPE_ENGINE;
                break;
            case 4:
                result = FOURTH_TYPE_ENGINE;
                break;
            case 5:
                result = FIFTH_TYPE_ENGINE;
                break;
            default:
                result = "The fuel type engine  is set incorrectly";
                break;
        }
        return result;
    }
}
