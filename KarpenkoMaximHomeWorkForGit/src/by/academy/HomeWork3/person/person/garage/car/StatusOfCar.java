package by.academy.HomeWork3.person.person.garage.car;

public interface StatusOfCar {


    //abra-kadabra
    Boolean getStatusToStartEngine();//engine

    void setStatusToStartEngine(Boolean statusToStartEngine);//engine


    void getStatusToStartEngine(Boolean statusToStartEngine);//engine

    Boolean checkStatusForWheel(double statusForWheel);//Wheels

    Boolean checkTypeEngineAndTypeFuel(Integer numberOfEngineFuel, Integer numberOfEngineType);//engine

    //car
    String getGpsData();


    void setGpsData(String gpsData);


    //doors
    Boolean getStatusTheDoorIsClosed();

    Boolean getStatusTheLockInDoorIsClosed();

    void setStatusTheLockInDoorIsClosed(Boolean statusTheLockInDoorIsClosed);

    void setStatusTheDoorIsClosed(Boolean statusTheDoorIsClosed);

    //seat
    Integer getNumberSeatsInCar();//idea не коректно можно сама вставлять из буфера    О______О

    void setNumberSeatsInCar(Integer numberSeatsInCar);

    Integer getNumberOfEmptySeatsInCar();

    void setNumberOfEmptySeatsInCar(Integer numberOfEmptySeatsInCar);


    //tank
    void setMaxLevelFuelInTheTank(Double maxLevelFuelInTheTank);

    Double getFuelLevelInTheTank();

    void setFuelLevelInTheTank(Double fuelLevelInTheTank);

    Boolean noFuelInTheTank(Double fuelLevelInTheTank);

    void lowLevelFuelInTheTank();

    Boolean runningOutOfFuel(Double fuelLevelInTheTank);

    Double getMaxLevelFuelInTheTank();

    //lock
    Boolean openOrCloseLockForCar(String uniqueIdKeyForCar);


}
