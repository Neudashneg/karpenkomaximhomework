package by.academy.HomeWork3.person.passengers;

public class Passengers {
    private Integer numberPhoneForPassenger;
    private String firstNamePassenger;
    private String secondNamePassenger;
    private Boolean areThereAnyPassengers;
    private String gpsData;

    public String getGpsData() {
        return gpsData;
    }

    public Passengers(String firstNamePassenger, String secondNamePassenger, Integer numberPhoneForPassenger) {
        this.firstNamePassenger = firstNamePassenger;
        this.secondNamePassenger = secondNamePassenger;
        this.numberPhoneForPassenger = numberPhoneForPassenger;
    }

    public Boolean getAreThereAnyPassengers() {
        return areThereAnyPassengers;
    }
}
