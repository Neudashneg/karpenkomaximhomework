package by.academy.HomeWork1.task2.calculatorwithmath;


import by.academy.HomeWork1.task2.calculatorwithoperator.ICalculator;

import java.io.Serializable;


public class CalculatorWithMath implements ICalculator {


    public double division(double firstVariable, double secondVariable) {
        double result = firstVariable / secondVariable;
        return result;
    }

    public double multiplication(double firstVariable, double secondVariable) {
        double result = firstVariable * secondVariable;
        return result;
    }

    public double summation(double firstVariable, double secondVariable) {
        double result = firstVariable + secondVariable;
        return result;
    }

    public double subtraction(double firstVariable, double secondVariable) {
        double result = firstVariable - secondVariable;
        return result;
    }

    @Override
    public double exponentiation(double number, int degree) {
        return 0;
    }

    public double exponentiationMath(double number, double degree) {
        return Math.pow(number, degree);
    }

    public double numberModule(double number) {
        return Math.abs(number);
    }

    @Override
    public Serializable extractionOfRoots(double number, int degreeOfTheRoot) {
        return null;
    }


    public double extractionOfRoots(double number, double degreeOfTheRoot) {
        //корень числа не что иное как возведения числа в степень еденица деленная на число корней, поэтому используем Math.pow(number,1/degreeOfTheRoot)
        return Math.pow(number,1/ degreeOfTheRoot);
    }

}
