package by.academy.HomeWork1.task2.calculatorwithoperator;

import java.io.Serializable;

public interface ICalculator {
    double division(double firstVariable, double secondVariable);// /

    double multiplication(double firstVariable, double secondVariable);// *

    double summation(double firstVariable, double secondVariable);// +

    double subtraction(double firstVariable, double secondVariable);// -

    double exponentiation(double number, int degree);// ^

    double exponentiationMath(double number, double degree);// ^

    double numberModule(double number); // module

    Serializable extractionOfRoots(double number, int degreeOfTheRoot);// 1/(^)

    double extractionOfRoots(double number, double degreeOfTheRoot);// 1/(^)

}
