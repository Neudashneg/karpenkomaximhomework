package by.academy.HomeWork1.task2;

import by.academy.HomeWork1.task2.calculatorwithcounter.CalculatorWithCounter;
import by.academy.HomeWork1.task2.calculatorwithmath.CalculatorWithMath;
import by.academy.HomeWork1.task2.calculatorwithoperator.CalculatorWithOperator;
import by.academy.HomeWork1.task2.calculatorwithoperator.ICalculator;

/**
 * For a calculator that will perform the functions of calculators CalculatorWithOperator
 * и CalculatorWithMath use the constructor with a link to the interface ICalculator and
 * overloading the interface that returns the operation counter, and the value of the executed method.
 */

public class CalculatorWithCounterMain {
    public static void main(String[] args) {

        ICalculator calculatorWithCounter = (ICalculator) new CalculatorWithCounter(new CalculatorWithOperator());
//        ICalculator calculatorWithCounter = new CalculatorWithCounter();
//        Error:(8, 55) java: constructor CalculatorWithCounter in class by.academy.HomeWork1.task2.calculatorwithcounter.CalculatorWithCounter cannot be applied to given types;
//        required: by.academy.HomeWork1.task2.calculatorwithoperator.ICalculator
//        found: no arguments
//        reason: actual and formal argument lists differ in length
        CalculatorWithCounter calculatorWithCounter2 = new CalculatorWithCounter(new CalculatorWithMath());
        var result = calculatorWithCounter.summation(4.1, calculatorWithCounter.summation(calculatorWithCounter.multiplication(15d, 7d), calculatorWithCounter.exponentiation(calculatorWithCounter.division(28, 5), 2)));
        var result2 = calculatorWithCounter2.summation(4.1, calculatorWithCounter2.summation(calculatorWithCounter2.multiplication(15d, 7d), calculatorWithCounter2.exponentiationMath(calculatorWithCounter2.division(28, 5), 2)));
        System.out.println("The result of using CalculatorWithOperator "+result + " number of operations to get the result " + ((CalculatorWithCounter) calculatorWithCounter).getCountOperation());
        System.out.println("The result of using CalculatorWithMath     " +result2 + " number of operations to get the result " + calculatorWithCounter2.getCountOperation());
        System.out.println(calculatorWithCounter2.getCountOperation());

    }
}
