package by.academy.HomeWork1.task2.calculatorwithcounter;

import by.academy.HomeWork1.task2.calculatorwithoperator.ICalculator;

import java.io.Serializable;

public class CalculatorWithCounter {
    private int countOperation = 0;
    private ICalculator iCalculator;

    public int getCountOperation() {
        return countOperation;
    }

    public CalculatorWithCounter(ICalculator iCalculator) {
//public CalculatorWithCounter() {
//Exception in thread "main" java.lang.NullPointerException
        this.countOperation++;
        this.iCalculator = iCalculator;

    }

    public double division(double firstVariable, double secondVariable) {
        this.countOperation++;
        return iCalculator.division(firstVariable, secondVariable);
    }


    public double multiplication(double firstVariable, double secondVariable) {
        this.countOperation++;
        return iCalculator.multiplication(firstVariable, secondVariable);
    }


    public double summation(double firstVariable, double secondVariable) {
        this.countOperation++;
        return iCalculator.summation(firstVariable, secondVariable);
    }


    public double subtraction(double firstVariable, double secondVariable) {
        this.countOperation++;
        return iCalculator.subtraction(firstVariable, secondVariable);
    }

    public double exponentiation(double number, int degree) {
        this.countOperation++;
        return iCalculator.exponentiation(number, degree);
    }


    public double numberModule(double number) {
        this.countOperation++;
        return iCalculator.numberModule(number);
    }

    public Serializable extractionOfRoots(double number, int degreeOfTheRoot) {
        this.countOperation++;
        return iCalculator.extractionOfRoots(number, degreeOfTheRoot);
    }

    public double exponentiationMath(double number, double degree) {
        this.countOperation++;
        return iCalculator.exponentiationMath(number, degree);
    }


    public double extractionOfRoots(double number, double degreeOfTheRootWithMath) {
        this.countOperation++;
        return iCalculator.extractionOfRoots(number, degreeOfTheRootWithMath);
    }
}
