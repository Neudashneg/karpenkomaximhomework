package by.academy.HomeWork1.task2;


import by.academy.HomeWork1.task2.calculatorwithoperator.CalculatorWithOperator;
import by.academy.HomeWork1.task2.calculatorwithoperator.ICalculator;

import java.io.Serializable;

/**
 * CalculatorWithOperator
 * * Calculator using mathematical operators.
 * This calculator almost does not use the Math library.
 * This calculator can add, multiply, divide,* subtract raise to a degree return a number modulo
 * limited to only the ranges of values of type Double.
 * This calculator can also calculate the roots of an integer
 * conditionally any degree of selection, to speed up the search for the root
 * using the Math function.round, which introduces an error in the result.
 * Without the Math function.round calculating three roots out of 27 more than five minutes
 * With the Math function.round about a second but the margin of error was 0.6%.
 *
 * CalculatorWithMath
 *This calculator can perform the same actions as the previous one only raise to a degree extract
 * roots from any number of fractional degree limited only by the boundaries of the Double value.
 *
 */
public class CalculatorWithOperatorMain implements ICalculator {
    public static void main(String[] arg) {
        CalculatorWithOperator calculatorWithOperator = new CalculatorWithOperator();
        System.out.println(calculatorWithOperator.extractionOfRoots(81, 4));//does not want to calculate the root of 4 degrees/ / fixed
        System.out.println(calculatorWithOperator.extractionOfRoots(6231235, -1));
        var result = calculatorWithOperator.summation(4.1, calculatorWithOperator.summation(calculatorWithOperator.multiplication(15d, 7d), calculatorWithOperator.exponentiation(calculatorWithOperator.division(28, 5), 2)));
        System.out.println(result);
        result = result / 0;
        System.out.println(result);
        result = result / 0.0d;
        System.out.println(result);


    }

    @Override
    public double division(double firstVariable, double secondVariable) {
        return 0;
    }

    @Override
    public double multiplication(double firstVariable, double secondVariable) {
        return 0;
    }

    @Override
    public double summation(double firstVariable, double secondVariable) {
        return 0;
    }

    @Override
    public double subtraction(double firstVariable, double secondVariable) {
        return 0;
    }

    @Override
    public double exponentiation(double number, int degree) {
        return 0;
    }

    @Override
    public double exponentiationMath(double number, double degree) {
        return 0;
    }

    @Override
    public double numberModule(double number) {
        return 0;
    }

    @Override
    public Serializable extractionOfRoots(double number, int degreeOfTheRoot) {
        return null;
    }

    @Override
    public double extractionOfRoots(double number, double degreeOfTheRoot) {
        return 0;
    }
}


