package by.academy.HomeWork2.task2;

public class Main {
    public static void main(String[] args) {
        GamesWithDynamicsMassive gamesWithDynamicsMassive = new GamesWithDynamicsMassive();
        gamesWithDynamicsMassive.searchForADuplicateNumberThatIsNotAMultipleOfTwo("20,1,-1,2,-2,3,3,5,5,1,2,4,20,4,-1,-2,5");
        gamesWithDynamicsMassive.searchForADuplicateNumberThatIsNotAMultipleOfTwo("1,1,2,-2,5,2,4,4,-1,-2,5");
        gamesWithDynamicsMassive.searchForADuplicateNumberThatIsNotAMultipleOfTwo("20,1,1,2,2,3,3,5,5,4,20,4,5");
        gamesWithDynamicsMassive.searchForADuplicateNumberThatIsNotAMultipleOfTwo("10");
        gamesWithDynamicsMassive.searchForADuplicateNumberThatIsNotAMultipleOfTwo("1,1,1,1,1,1,10,1,1,1,1");
        gamesWithDynamicsMassive.searchForADuplicateNumberThatIsNotAMultipleOfTwo("5,4,3,2,1,5,4,3,2,10,10");

    }
}
