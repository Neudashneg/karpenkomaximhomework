package by.academy.HomeWork2.task2;

import java.util.ArrayList;


/*
 * 1 creating a dynamic list array
 *
 *3 Use the specified array to find a number that occurs an odd number of times
 * (There will always be only one number that occurs an odd number of times):
 *
 * */
public class GamesWithDynamicsMassive {
    private ArrayList<Integer> dinamicMassive = new ArrayList<>(1);


    public GamesWithDynamicsMassive() {

    }


//1. we make an array of numbers from a string, comma-separated integers between them

    public ArrayList<Integer> CreatorDynamicsMassive(String strForArray) {
        byte commaCounter = 0;
        int[] commaAddresses = new int[strForArray.length()];
        //making a number in the string view
        for (int z = 0; z < strForArray.length(); z++) {
            if (strForArray.charAt(z) == ',') {             //first we count the number of commas
                commaAddresses[commaCounter] = z;
                commaCounter++;
            }
        }
        if (commaCounter == 0) {                        //making an array
            dinamicMassive.add(Integer.valueOf(strForArray));
        } else {
            for (int i = 0; i < commaCounter + 1; ) {
                for (int j = 0; j < strForArray.length(); j++) {
                    if (strForArray.charAt(j) == ',') {//finding a comma
                        if (i == 0) {
                            dinamicMassive.add(i, Integer.valueOf(strForArray.substring(0, commaAddresses[i])));
                        } else if (i != 0 && i < commaCounter) {
                            dinamicMassive.add(i, Integer.valueOf(strForArray.substring(commaAddresses[i - 1] + 1, commaAddresses[i])));
                        } else if (i != 0 && i == commaCounter) {
                            dinamicMassive.add(i, Integer.valueOf(strForArray.substring(commaAddresses[i - 1] + 1, strForArray.length() - 1 + 1)));
                        }
                        i++;
                    }
                }
            }
        }
        return dinamicMassive;
    }
    public void searchForADuplicateNumberThatIsNotAMultipleOfTwo(String str) {
        CreatorDynamicsMassive(str);
        Integer results = null;

        for (int i = 0; i < dinamicMassive.size(); i++) {//number of elements
            int matchCounter = 0;
            for (int j = 0; j < dinamicMassive.size(); j++) {
                if (dinamicMassive.get(i) == dinamicMassive.get(j)) {
                    matchCounter++;
                }
            }
            if (matchCounter % 2 != 0) {
                results = dinamicMassive.get(i);
            }
        }
        System.out.println(str);
        System.out.println(CreatorDynamicsMassive(str));
        System.out.println(results);
    }
}

