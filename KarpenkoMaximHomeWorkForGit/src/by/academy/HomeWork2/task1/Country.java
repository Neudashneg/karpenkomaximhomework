package by.academy.HomeWork2.task1;

public enum Country implements GetTheAreaAndPopulationOfCountry {
        Belarus(207600, 9513000), Russia(17098250, 144096812),
        Canada(9984670, 35851774), Australia(7741220, 23781169),
        Madagascar(587295, 24235390), Iceland(103000, 330823),
        Greenland(410450, 56114), Monaco(2, 37731),
        Malta(320, 431333), Singapore(719, 5535002);
        private int countryPopulation;
        private int areaOfTheCountry;

        Country(int areaOfTheCountry, int countryPopulation) {
            this.areaOfTheCountry=areaOfTheCountry;
            this.countryPopulation=countryPopulation;
        }

    @Override
    public int getAreaOfCountry() {
            return areaOfTheCountry;

    }

    @Override
    public int getPopulationOfCountry() {
        return countryPopulation;
    }
}
