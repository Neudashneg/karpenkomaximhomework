package by.academy.HomeWork2.task1;

/**
 * List of 10 countries with populations and area of territories.
 * List created in the "Country" enum
 * To add population and area values to the enumeration, use the constructor with parameters.
 * To get population and area values, use the "GetTheAreaAndPopulationOfCountry" interface.
 * To compare the area of countries in the "CountryAreaComparator" class, we use the internal interface "Comparator" Java with the <Country>parameter.
 */

public class CountryMain {
    public static void main(String[] args) {
        Country firstCountry = Country.Belarus;
        System.out.println(firstCountry);
        Country firstCountryOfString = Country.valueOf("Belarus");
        Country sixthCountry = Country.Iceland;
        Country seventhCountry = Country.Greenland;
        System.out.println(firstCountryOfString == firstCountry);
        System.out.println("Area of  " + sixthCountry + " " + sixthCountry.getAreaOfCountry() + " sq. km.");
        System.out.println("Population of  " + seventhCountry + " " + seventhCountry.getPopulationOfCountry() + " number of people");
        System.out.println(compareResult(Country.Iceland, Country.Greenland));
        System.out.println(compareResult(Country.Canada, Country.Australia));
        System.out.println(compareResult(Country.Madagascar, Country.Monaco));
        System.out.println(compareResult(Country.Belarus, Country.Singapore));
        System.out.println(compareResult(Country.Malta, Country.Malta));
        System.out.println(compareResult(Country.Russia, Country.Canada));
    }

    public static String compareResult(Country o1, Country o2) {
        CountryAreaComparator countryAreaComparator = new CountryAreaComparator();
        if (countryAreaComparator.compare(o1, o2) > 0) {
            return "Area of  " + o1 + " more area of  " + o2;
        } else if (countryAreaComparator.compare(o1, o2) < 0) {
            return "Area of  " + o1 + " less area of  " + o2;
        } else {
            return "Areas of  " + o1 + " ands " + o2 + "equal";
        }
    }

}
