package by.academy.HomeWork2.task1;

public interface GetTheAreaAndPopulationOfCountry {
    int getAreaOfCountry();
     int getPopulationOfCountry();
}
