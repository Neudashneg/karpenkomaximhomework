package by.academy.HomeWork2.task1;

import java.util.Comparator;

public class CountryAreaComparator implements Comparator<Country> {
    /**
     * Example with the internal interface "Comparator" Java with
     * a public int compareTo(House anotherHouse)
     * {
     * if (this.area == anotherHouse.area) {
     * return 0;
     * } else if (this.area < anotherHouse.area) {
     * return -1;
     * } else {
     * return 1;
     *
     * @return
     */


    @Override
    public int compare(Country o1, Country o2) {


        if (o1.getAreaOfCountry() > o2.getAreaOfCountry()) {
            return 1;
        }
        if (o1.getAreaOfCountry() < o2.getAreaOfCountry()) {
            return -1;
        } else {
            return 0;
        }
    }
}
